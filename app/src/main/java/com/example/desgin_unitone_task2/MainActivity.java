package com.example.desgin_unitone_task2;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.ColorUtils;
import androidx.viewpager.widget.ViewPager;

import com.example.desgin_unitone_task2.adapters.MainActivityViewPagerAdapter;
import com.example.desgin_unitone_task2.fragments.*;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(ColorUtils.setAlphaComponent(Color.TRANSPARENT,15));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }


        bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation_view);
        viewPager = findViewById(R.id.activity_main_view_pager);


        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AthcarListFragment()).commit();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bottom_navigation_menu_athcar_list:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AthcarListFragment()).commit();
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.bottom_navigation_menu_favorite:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new FavoriteAthcarListFragment()).commit();
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.bottom_navigation_menu_ramadan_questions:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new RamadanQuestionsFragment()).commit();
                        viewPager.setCurrentItem(2);
                        return true;
                    case R.id.bottom_navigation_menu_about_app:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AboutAppFragment()).commit();
                        viewPager.setCurrentItem(3);
                        return true;

                }
                return false;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                System.out.println(""+position);
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_athcar_list);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_favorite);
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_ramadan_questions);
                        break;
                    case 3:
                        bottomNavigationView.setSelectedItemId(R.id.bottom_navigation_menu_about_app);
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPager.setOffscreenPageLimit(4);
        MainActivityViewPagerAdapter pagerAdapter = new MainActivityViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });

    }


}