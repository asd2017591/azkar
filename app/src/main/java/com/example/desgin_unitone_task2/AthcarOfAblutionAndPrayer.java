package com.example.desgin_unitone_task2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.ColorUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.adapters.AthcarAdapter;
import com.example.desgin_unitone_task2.fragments.AthcarListFragment;
import com.example.desgin_unitone_task2.items.Athcar;

import java.util.ArrayList;

public class AthcarOfAblutionAndPrayer extends AppCompatActivity {
    public final static  String ATHCHAR_INTENT_TAG = "ATHCHAR";
    ArrayList<Athcar> athcarArrayList;
    AthcarAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.athcar_of_ablution_and_prayer_layout);

        Toolbar toolbar = findViewById(R.id.athcar_of_ablution_and_prayer_layout_toolbar);
        TextView athcar_number_title_TextView = findViewById(R.id.athcar_of_ablution_and_prayer_layout_tv_athcar_number);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(ColorUtils.setAlphaComponent(Color.TRANSPARENT, 15));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        RecyclerView recyclerView = findViewById(R.id.athcar_of_ablution_and_prayer_layout_recyclerview);

        Athcar athcar = getIntent().getParcelableExtra(AthcarListFragment.ATHCHAR_OBJECT_INTENT_TAG);

        athcarArrayList = new ArrayList<>();
        if(athcar != null){
            athcarArrayList =  athcarType(athcar);
            athcar_number_title_TextView.setText(athcar.getNumber());
        }

          adapter = new AthcarAdapter(athcarArrayList, new AthcarAdapter.OnAthcarClickListener() {


            @Override
            public void OnItemClick(Athcar atchar) {
                startActivity(new Intent(getBaseContext(), PrayersDetails.class).putExtra(ATHCHAR_INTENT_TAG,atchar));
            }

            @Override
            public void OnImageFavoriteClick(Athcar atchar, int position) {
                Athcar athcar1 = athcarArrayList.get(position);
                if(athcar1.getLike() == Athcar.Like.LIKE){
                    athcar1.setLike(Athcar.Like.UNLIKE);

                }else {
                    athcar1.setLike(Athcar.Like.LIKE);
                }
                athcarArrayList.remove(position);
                athcarArrayList.add(position,athcar1);
                adapter.notifyItemChanged(position);
            }
        });

        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public ArrayList<Athcar> athcarType(Athcar athcar){
        ArrayList<Athcar> athcars = new ArrayList<>();
        switch (athcar.getTitle()){
            case "أذكــــار الوضـــوء والصــــــلاة":
                AddAthcarOfAblutionAndPrayer(athcars);
                break;
            case "أدعــــــية الطعام والصـــــيام":
                AddPrayersFoodAndFasting(athcars);
                break;
            default:
                AddPrayersHajjAndUmrah(athcars);
                break;
        }
        return athcars;
    }
    //الوضوء والصلاة
    public void AddAthcarOfAblutionAndPrayer(ArrayList<Athcar> arrayList){
       arrayList.add(new Athcar( "أذكار الوضوء", "80 ذكر"));
       arrayList.add(new Athcar( "أذكار ما بعد الصلاة", "26 ذكر", Athcar.Like.LIKE));
       arrayList.add(new Athcar( "أذكار ما قبل الصلاة", "25 ذكر"));
    }
    //أدعــــــية الطعام والصـــــيام
    public void AddPrayersFoodAndFasting(ArrayList<Athcar> arrayList){
        arrayList.add(new Athcar( "أذكار السحور", "38 ذكر"));
        arrayList.add(new Athcar( "أذكار قبل الأكل", "25 ذكر", Athcar.Like.LIKE));
        arrayList.add(new Athcar( "أذكار بعد الأكلة", "17 ذكر"));
        arrayList.add(new Athcar( "أذكار الإفطار", "11 ذكر"));
    }
    //أدعــــية الحـــج والعمرة
    public void AddPrayersHajjAndUmrah(ArrayList<Athcar> arrayList){
        arrayList.add(new Athcar( "أذكار يوم عرفة", "30 ذكر"));
        arrayList.add(new Athcar( "أذكار يوم النحر", "20 ذكر", Athcar.Like.LIKE));
        arrayList.add(new Athcar( "أذكار طواف", "15 ذكر"));

    }

}