package com.example.desgin_unitone_task2;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.viewpager.widget.ViewPager;

import com.example.desgin_unitone_task2.adapters.PrayerFragmentPagerAdapter;
import com.example.desgin_unitone_task2.fragments.PrayersDetailsFragment;
import com.example.desgin_unitone_task2.items.Athcar;

import java.util.ArrayList;

public class PrayersDetails extends AppCompatActivity implements PrayersDetailsFragment.OnPagerImageClickListener {
    ViewPager viewPager;
    ArrayList<Athcar> athcarArrayList;
    PrayerFragmentPagerAdapter adapter;
    TextView prayersTitleTextView;
    Athcar athcar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prayers_details_layout);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.setStatusBarColor(ColorUtils.setAlphaComponent(Color.TRANSPARENT, 15));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
        prayersTitleTextView = findViewById(R.id.prayers_details_layout_tv_number_of_prayers);
        Toolbar toolbar = findViewById(R.id.prayers_details_layout_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        viewPager = findViewById(R.id.prayers_details_layout_view_pager);
        athcarArrayList = new ArrayList<>();
        athcarArrayList.add(new Athcar("10", getString(R.string.prayer), "السنة النبوية", Athcar.Like.UNLIKE));
        athcarArrayList.add(new Athcar("100", getString(R.string.prayer), "السنة النبوية", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar("33", getString(R.string.prayer), "السنة النبوية", Athcar.Like.UNLIKE));
        athcarArrayList.add(new Athcar("3", getString(R.string.prayer), "السنة النبوية", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar("1", getString(R.string.prayer), "السنة النبوية", Athcar.Like.UNLIKE));

        adapter = new PrayerFragmentPagerAdapter(getSupportFragmentManager(), athcarArrayList);
        viewPager.setAdapter(adapter);

        athcar = getIntent().getParcelableExtra(AthcarOfAblutionAndPrayer.ATHCHAR_INTENT_TAG);

        if (athcar != null) {
            toolbar.setTitle(athcar.getTitle());
            changeTitle(" ذكر " + 1 + " من " + athcar.getNumber());
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (athcar != null) {
                    changeTitle(" ذكر " + (position + 1) + " من " + athcar.getNumber());
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void back(View view) {
        int currentItem = viewPager.getCurrentItem();
        if (currentItem >= 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    public void next(View view) {
        int currentItem = viewPager.getCurrentItem();
        if (currentItem < athcarArrayList.size()) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onImageClick(int position) {
        System.out.println("before" + athcarArrayList.get(0).getLike().toString());
        Athcar athcar = athcarArrayList.get(position);
        if (athcar.getLike() == Athcar.Like.LIKE)
            athcar.setLike(Athcar.Like.UNLIKE);
        else
            athcar.setLike(Athcar.Like.LIKE);
        athcarArrayList.remove(position);
        athcarArrayList.add(position, athcar);
        adapter.notifyDataSetChanged();
    }

    @SuppressLint("ResourceAsColor")
    private void changeTitle(String title) {
        Spannable spannable = new SpannableString(title);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.purple)), 5, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.purple)), 10, 12, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        prayersTitleTextView.setText(spannable);
    }

}