package com.example.desgin_unitone_task2.items;

public class RamadanQuestion {
    private String question;
    private String answer;
    private VISIBILITY visibility = VISIBILITY.UNVISITABLE;

    public static enum VISIBILITY {
        VISITABLE,
        UNVISITABLE
    }

    public RamadanQuestion(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public RamadanQuestion(String question, String answer, VISIBILITY visibility) {
        this.question = question;
        this.answer = answer;
        this.visibility = visibility;
    }

    public VISIBILITY getVisibility() {
        return visibility;
    }

    public void setVisibility(VISIBILITY visibility) {
        this.visibility = visibility;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
