package com.example.desgin_unitone_task2.items;

import android.os.Parcel;
import android.os.Parcelable;

public class Athcar implements Parcelable {
    public static enum Like {
        LIKE,
        UNLIKE
    }

    public Like getLike() {
        return like;
    }

    public void setLike(Like like) {
        this.like = like;
    }

    private int drawableId;
    private String title;
    private String number;
    private Like like = Like.UNLIKE;
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Athcar( String number,String title, String source, Like like) {
        this.title = title;
        this.number = number;
        this.like = like;
        this.source = source;
    }

    public Athcar(int drawableId, String title, String number, Like like) {
        this.drawableId = drawableId;
        this.title = title;
        this.number = number;
        this.like = like;
    }
    public Athcar( String title, String number, Like like) {

        this.title = title;
        this.number = number;
        this.like = like;
    }


    public Athcar(String title, String number) {
        this.title = title;
        this.number = number;
    }

    public Athcar(int drawableId, String title, String number) {
        this.drawableId = drawableId;
        this.title = title;
        this.number = number;
    }

    protected Athcar(Parcel in) {
        drawableId = in.readInt();
        title = in.readString();
        number = in.readString();
    }

    public static final Creator<Athcar> CREATOR = new Creator<Athcar>() {
        @Override
        public Athcar createFromParcel(Parcel in) {
            return new Athcar(in);
        }

        @Override
        public Athcar[] newArray(int size) {
            return new Athcar[size];
        }
    };

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(drawableId);
        dest.writeString(title);
        dest.writeString(number);
    }
}
