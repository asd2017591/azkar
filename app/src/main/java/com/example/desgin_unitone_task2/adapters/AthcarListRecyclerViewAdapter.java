package com.example.desgin_unitone_task2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.desgin_unitone_task2.items.Athcar;
import com.example.desgin_unitone_task2.R;

import java.util.ArrayList;

public class AthcarListRecyclerViewAdapter extends RecyclerView.Adapter {
    ArrayList<Athcar> athcars;
    OnAthcarItemClickListener onAthcarItemClickListener;

    public AthcarListRecyclerViewAdapter(ArrayList<Athcar> athcars, OnAthcarItemClickListener onAthcarItemClickListener) {
        this.onAthcarItemClickListener = onAthcarItemClickListener;
        this.athcars = athcars;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.athcar_list_recyclerview_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.bind(athcars.get(position));
    }

    @Override
    public int getItemCount() {
        return athcars.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView drawable;
        TextView number;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.athcar_list_recyclerview_item_tv_title);
            drawable = itemView.findViewById(R.id.athcar_list_recyclerview_item_iv);
            number = itemView.findViewById(R.id.athcar_list_recyclerview_item_tv_number);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAthcarItemClickListener.OnItemClick(athcars.get(getAdapterPosition()));
                }
            });


        }

        void bind(Athcar athcar) {
            drawable.setImageResource(athcar.getDrawableId());
            title.setText(athcar.getTitle());
            number.setText(athcar.getNumber());
        }
    }
    public interface OnAthcarItemClickListener{
        void OnItemClick(Athcar atchar);
    }
}
