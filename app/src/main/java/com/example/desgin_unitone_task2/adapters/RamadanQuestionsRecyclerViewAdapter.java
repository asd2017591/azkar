package com.example.desgin_unitone_task2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.R;
import com.example.desgin_unitone_task2.items.RamadanQuestion;

import java.util.ArrayList;

public class RamadanQuestionsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int last_question_click = -1;
    ArrayList<RamadanQuestion> questions;
    OnQuestionClickListener onQuestionClickListener;

    public RamadanQuestionsRecyclerViewAdapter(ArrayList<RamadanQuestion> questions, OnQuestionClickListener onQuestionClickListener) {
        this.questions = questions;
        this.onQuestionClickListener = onQuestionClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ramadan_questions_recyclerview_item_layout, parent, false);
        return new QuestionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        QuestionHolder viewHolder = (QuestionHolder) holder;
        viewHolder.bind(questions.get(position));
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    class QuestionHolder extends RecyclerView.ViewHolder {
        TextView question;
        TextView answer;

        public QuestionHolder(@NonNull final View itemView) {
            super(itemView);
            question = itemView.findViewById(R.id.ramadan_questions_recyclerview_item_layout_tv_question);
            answer = itemView.findViewById(R.id.ramadan_questions_recyclerview_item_layout_tv_answer);

            question.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onQuestionClickListener.OnQuestionClick(getAdapterPosition(),last_question_click);
                    last_question_click = getAdapterPosition();


                }
            });


        }


        void bind(RamadanQuestion RamadanQuestion) {
            question.setText(RamadanQuestion.getQuestion());
            answer.setText(RamadanQuestion.getAnswer());
            if (RamadanQuestion.getVisibility() == com.example.desgin_unitone_task2.items.RamadanQuestion.VISIBILITY.VISITABLE) {
                answer.setVisibility(View.VISIBLE);
                question.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.purple));
            } else {
                answer.setVisibility(View.GONE);
                question.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.light_blue));
            }
        }
    }

    public interface OnQuestionClickListener {
        void OnQuestionClick(int position, int lastClick);
    }
}

