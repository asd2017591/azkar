package com.example.desgin_unitone_task2.adapters;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.desgin_unitone_task2.fragments.PrayersDetailsFragment;
import com.example.desgin_unitone_task2.items.Athcar;

import java.util.ArrayList;

public class PrayerFragmentPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<Athcar> athcarArrayList ;
    public PrayerFragmentPagerAdapter(@NonNull FragmentManager fm, ArrayList<Athcar>athcarArrayList) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.athcarArrayList = athcarArrayList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return PrayersDetailsFragment.newInstance(athcarArrayList.get(position),position);

    }

    @Override
    public int getCount() {
        return athcarArrayList.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
