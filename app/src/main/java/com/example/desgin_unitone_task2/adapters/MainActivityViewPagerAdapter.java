package com.example.desgin_unitone_task2.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.desgin_unitone_task2.fragments.AboutAppFragment;
import com.example.desgin_unitone_task2.fragments.AthcarListFragment;
import com.example.desgin_unitone_task2.fragments.FavoriteAthcarListFragment;
import com.example.desgin_unitone_task2.fragments.RamadanQuestionsFragment;


public class MainActivityViewPagerAdapter extends FragmentStatePagerAdapter {


    public MainActivityViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 4;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new AthcarListFragment();
            case 1:
                return new FavoriteAthcarListFragment();

            case 2:
                return new RamadanQuestionsFragment();

            case 3:
                return new AboutAppFragment();


        }
        return null;
    }

}