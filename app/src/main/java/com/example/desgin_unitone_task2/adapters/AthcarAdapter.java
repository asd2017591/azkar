package com.example.desgin_unitone_task2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.R;
import com.example.desgin_unitone_task2.items.Athcar;

import java.util.ArrayList;


public class AthcarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int UNLIKE = 0;
    private int LIKE = 1;
    ArrayList<Athcar> athcars;
    OnAthcarClickListener onAthcarClickListener;

    public AthcarAdapter(ArrayList<Athcar> athcars, OnAthcarClickListener onAthcarClickListener) {
        this.onAthcarClickListener = onAthcarClickListener;
        this.athcars = athcars;
    }

    @Override
    public int getItemViewType(int position) {
        if (athcars.get(position).getLike() == Athcar.Like.LIKE)
            return LIKE;
        else
            return UNLIKE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == LIKE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.athcar_recyclerview_favorite_item_layout, parent, false);
            return new FavoriteViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.athcar_recyclerview_unfavorite_item__layout, parent, false);
            return new UnFavoriteViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                FavoriteViewHolder favoriteViewHolder = (FavoriteViewHolder)holder;
                favoriteViewHolder.bind(athcars.get(position));
                break;

            case 0:
                UnFavoriteViewHolder unFavoriteViewHolder  = (UnFavoriteViewHolder)holder;
                unFavoriteViewHolder.bind(athcars.get(position));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return athcars.size();
    }

    class FavoriteViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView number;
        View linearLayoutTitleAndNumber;
        View imageViewFavorite;
        public FavoriteViewHolder(@NonNull final View itemView) {
            super(itemView);

                title = itemView.findViewById(R.id.athcar_recyclerview_favorite_item_tv_title);
                number = itemView.findViewById(R.id.athcar_recyclerview_favorite_item_tv_number);
                linearLayoutTitleAndNumber = itemView.findViewById(R.id.athcar_recyclerview_favorite_item_linear_layout);
                imageViewFavorite = itemView.findViewById(R.id.athcar_recyclerview_favorite_item_iv_favorite);

            linearLayoutTitleAndNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAthcarClickListener.OnItemClick(athcars.get(getAdapterPosition()));
                }
            });
            imageViewFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAthcarClickListener.OnImageFavoriteClick(athcars.get(getAdapterPosition()),getAdapterPosition());
                }
            });
        }

        void bind(Athcar athcar) {
            title.setText(athcar.getTitle());
            number.setText(athcar.getNumber());
        }
    }
    class UnFavoriteViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView number;
        View linearLayoutTitleAndNumber;
        View imageViewUnFavorite;
        public UnFavoriteViewHolder(@NonNull final View itemView) {
            super(itemView);

                title = itemView.findViewById(R.id.athcar_recyclerview_unfavorite_item_tv_title);
                number = itemView.findViewById(R.id.athcar_recyclerview_unfavorite_item_tv_number);
                linearLayoutTitleAndNumber = itemView.findViewById(R.id.athcar_recyclerview_unfavorite_item_linear_layout);
                 imageViewUnFavorite = itemView.findViewById(R.id.athcar_recyclerview_unfavorite_item_iv_unfavorite);

            linearLayoutTitleAndNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAthcarClickListener.OnItemClick(athcars.get(getAdapterPosition()));
                }
            });
            imageViewUnFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAthcarClickListener.OnImageFavoriteClick(athcars.get(getAdapterPosition()),getAdapterPosition());
                }
            });


        }

        void bind(Athcar athcar) {
            title.setText(athcar.getTitle());
            number.setText(athcar.getNumber());
        }
    }

    public interface OnAthcarClickListener {
        void OnItemClick(Athcar atchar);
        void OnImageFavoriteClick(Athcar atchar,int position);
    }


}


