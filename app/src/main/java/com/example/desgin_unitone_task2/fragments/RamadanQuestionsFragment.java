package com.example.desgin_unitone_task2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.R;
import com.example.desgin_unitone_task2.items.RamadanQuestion;
import com.example.desgin_unitone_task2.SalahTimeActivity;
import com.example.desgin_unitone_task2.adapters.RamadanQuestionsRecyclerViewAdapter;

import java.util.ArrayList;

public class RamadanQuestionsFragment extends Fragment {
    private ArrayList<RamadanQuestion> ramadanQuestions;
    private RamadanQuestionsRecyclerViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ramadan_questions_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.ramadan_questions_layout_recyclerview);
        ImageView imageViewMosque = view.findViewById(R.id.ramadan_questions_layout_toolbar_iv_mosque);
        imageViewMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), SalahTimeActivity.class));
            }
        });
        ramadanQuestions = new ArrayList<>();

        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question1), getString(R.string.ramadan_questions_layout_tv_answer1)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question2), getString(R.string.ramadan_questions_layout_tv_answer2)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question3), getString(R.string.ramadan_questions_layout_tv_answer3)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question4), getString(R.string.ramadan_questions_layout_tv_answer4)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question5), getString(R.string.ramadan_questions_layout_tv_answer5)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question1), getString(R.string.ramadan_questions_layout_tv_answer1)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question2), getString(R.string.ramadan_questions_layout_tv_answer2)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question3), getString(R.string.ramadan_questions_layout_tv_answer3)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question4), getString(R.string.ramadan_questions_layout_tv_answer4)));
        ramadanQuestions.add(new RamadanQuestion(getString(R.string.ramadan_questions_layout_tv_question5), getString(R.string.ramadan_questions_layout_tv_answer5)));

        adapter = new RamadanQuestionsRecyclerViewAdapter(ramadanQuestions, new RamadanQuestionsRecyclerViewAdapter.OnQuestionClickListener() {
            @Override
            public void OnQuestionClick(int position, int last_question_click) {

                RamadanQuestion currentQuestion = ramadanQuestions.get(position);

                if (last_question_click == position) {
                    if (currentQuestion.getVisibility() == RamadanQuestion.VISIBILITY.UNVISITABLE) {
                        currentQuestion.setVisibility(RamadanQuestion.VISIBILITY.VISITABLE);
                    } else {
                        currentQuestion.setVisibility(RamadanQuestion.VISIBILITY.UNVISITABLE);
                    }
                    ramadanQuestions.remove(last_question_click);
                    ramadanQuestions.add(last_question_click, currentQuestion);
                } else {
                    currentQuestion.setVisibility(RamadanQuestion.VISIBILITY.VISITABLE);
                    ramadanQuestions.remove(position);
                    ramadanQuestions.add(position, currentQuestion);
                    if (last_question_click >= 0) {
                        RamadanQuestion lastQuestion = ramadanQuestions.get(last_question_click);
                        lastQuestion.setVisibility(RamadanQuestion.VISIBILITY.UNVISITABLE);
                        ramadanQuestions.remove(last_question_click);
                        ramadanQuestions.add(last_question_click, lastQuestion);
                    }
                    adapter.notifyItemChanged(last_question_click);
                }
                adapter.notifyItemChanged(position);

            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }
}
