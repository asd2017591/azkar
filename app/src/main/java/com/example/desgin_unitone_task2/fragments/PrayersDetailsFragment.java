package com.example.desgin_unitone_task2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.desgin_unitone_task2.R;
import com.example.desgin_unitone_task2.items.Athcar;

public class PrayersDetailsFragment extends Fragment {

    private static final String ARG_ATHCAR_OBJECT = "Athcar";
    private static final String ARG_ATHCAR_POSITION = "Position";


    private OnPagerImageClickListener onPagerImageClickListener;
    private String mNumber;
    private String mPrayer;
    private String mSource;
    private Athcar.Like mFavorite;
    private int mPosition;

    public PrayersDetailsFragment() {
        // Required empty public constructor
    }

    public static PrayersDetailsFragment newInstance(Athcar athcar, int position) {
        PrayersDetailsFragment fragment = new PrayersDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ATHCAR_OBJECT, athcar);
        args.putInt(ARG_ATHCAR_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPosition = getArguments().getInt(ARG_ATHCAR_POSITION);
            Athcar athcar = getArguments().getParcelable(ARG_ATHCAR_OBJECT);
            if (athcar != null) {
                mNumber = athcar.getNumber();
                mPrayer = athcar.getTitle();
                mSource = athcar.getSource();
                mFavorite = athcar.getLike();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_prayers_details, container, false);

        TextView number = view.findViewById(R.id.fragments_prayers_details_tv_prayer_number);
        TextView number_as_text = view.findViewById(R.id.fragments_prayers_details_tv_prayer_number_as_text);
        TextView prayerText = view.findViewById(R.id.fragments_prayers_details_tv_prayer_text);
        TextView prayerSource = view.findViewById(R.id.fragments_prayers_details_tv_prayer_source);
        final ImageView prayerImageViewLike = view.findViewById(R.id.fragments_prayers_details_iv_like_and_unlike);
        number.setText(mNumber);
        prayerText.setText(mPrayer);
        prayerSource.setText(mSource);
        number_as_text.setText(setNumberOfPrayersAsText(Integer.parseInt(mNumber)));
        if (mFavorite == Athcar.Like.LIKE)
            prayerImageViewLike.setBackgroundResource(R.drawable.favorite_athcar_bar);
        else
            prayerImageViewLike.setBackgroundResource(R.drawable.unfavorite_athcar_bar);

        prayerImageViewLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageClick(mPosition);
            }
        });


        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnPagerImageClickListener) {
            onPagerImageClickListener = (OnPagerImageClickListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implemented OnPagerImageClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onPagerImageClickListener = null;
    }

    public void onImageClick(int index) {
        if (onPagerImageClickListener != null) {
            onPagerImageClickListener.onImageClick(index);
        }
    }

    public interface OnPagerImageClickListener {
        void onImageClick(int position);
    }
    private String setNumberOfPrayersAsText(int number){
        switch (number){
            case 1:
                return "مرة";
            case 2:
                return "مرتان";
            case 3:
                return "ثلاث مرات";
            case 33:
                return "ثلاث وثلاثون مرة";
            case 100:
                return "مئة مرة";
            case 10:
                return "عشر مرات";
            default:
                return number+"مرات";
        }
    }

}