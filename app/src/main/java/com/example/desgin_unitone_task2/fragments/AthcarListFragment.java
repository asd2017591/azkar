package com.example.desgin_unitone_task2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.AthcarOfAblutionAndPrayer;
import com.example.desgin_unitone_task2.items.Athcar;
import com.example.desgin_unitone_task2.SalahTimeActivity;
import com.example.desgin_unitone_task2.adapters.AthcarListRecyclerViewAdapter;
import com.example.desgin_unitone_task2.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;

public class AthcarListFragment extends Fragment {
    public final static String ATHCHAR_OBJECT_INTENT_TAG = "athcar";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.athcar_list_layout,container,false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView =  view.findViewById(R.id.athcar_list_layout_recyclerview);
        ImageView  imageView_mosque = view.findViewById(R.id.athcar_list_layout_toolbar_iv_mosque);


        ArrayList <Athcar> athcars = new ArrayList<>();
        athcars.add(new Athcar(R.drawable.salah_icon,"أذكــــار الوضـــوء والصــــــلاة","10 أذكار"));
        athcars.add(new Athcar(R.drawable.food_icon,"أدعــــــية الطعام والصـــــيام","10 أذكار"));
        athcars.add(new Athcar(R.drawable.kaaba_icon,"أدعــــية الحـــج والعمرة","15 ذكر"));
        athcars.add(new Athcar(R.drawable.shirt_icon,"أذكــــار الملابس","4 أذكار"));
        athcars.add(new Athcar(R.drawable.shop_icon,"أدعــــية المعاملات","15 ذكر"));
        athcars.add(new Athcar(R.drawable.ill_icon,"أدعية المرض والضيق والحزن","18 ذكر"));
        athcars.add(new Athcar(R.drawable.baby_icon,"أدعية الولادة والوفاة","4 أذكار"));
        athcars.add(new Athcar(R.drawable.weather_icon,"أدعـــــية الطقس","4 أذكار"));
        athcars.add(new Athcar(R.drawable.girl_boy_icon,"أدعية الزواج والأولاد","15 ذكرر"));
        athcars.add(new Athcar(R.drawable.plane_icon,"أدعية السفر والسوق","12 ذكر"));
        athcars.add(new Athcar(R.drawable.quran_icon,"أذكـــــار إيمانية","3 أذكار"));
        athcars.add(new Athcar(R.drawable.animal_icon,"أدعية حول الحيوانات","18 ذكر"));
        athcars.add(new Athcar(R.drawable.moslem,"أدعــــية أخرى","25 ذكر"));
        AthcarListRecyclerViewAdapter adapter  =  new AthcarListRecyclerViewAdapter(athcars, new AthcarListRecyclerViewAdapter.OnAthcarItemClickListener() {
            @Override
            public void OnItemClick(Athcar atchar) {

                startActivity(new Intent(view.getContext(), AthcarOfAblutionAndPrayer.class).putExtra(ATHCHAR_OBJECT_INTENT_TAG,atchar));
            }
        });
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);


        imageView_mosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), SalahTimeActivity.class));
            }
        });
        MobileAds.initialize(view.getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });
        AdView mAdView = view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

}
