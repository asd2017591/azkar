package com.example.desgin_unitone_task2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.desgin_unitone_task2.AthcarOfAblutionAndPrayer;
import com.example.desgin_unitone_task2.PrayersDetails;
import com.example.desgin_unitone_task2.R;
import com.example.desgin_unitone_task2.SalahTimeActivity;
import com.example.desgin_unitone_task2.adapters.AthcarAdapter;
import com.example.desgin_unitone_task2.items.Athcar;

import java.util.ArrayList;

public class FavoriteAthcarListFragment extends Fragment {
    private AthcarAdapter adapter;
    private  ArrayList<Athcar> athcarArrayList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.athcar_favorite_layout,container,false);

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.athcar_favorite_layout_recyclerview);
        ImageView imageViewMosque = view.findViewById(R.id.athcar_favorite_layout_toolbar_iv_mosque);

        athcarArrayList = new ArrayList<>();
        athcarArrayList.add(new Athcar( "أذكار الوضوء", "80 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار ما بعد الصلاة", "26 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار ما قبل الصلاة", "25 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار السحور", "38 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار قبل الأكل", "25 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار بعد الأكلة", "17 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار الإفطار", "11 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار يوم عرفة", "30 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار يوم النحر", "20 ذكر", Athcar.Like.LIKE));
        athcarArrayList.add(new Athcar( "أذكار طواف", "15 ذكر", Athcar.Like.LIKE));

          adapter = new AthcarAdapter(athcarArrayList, new AthcarAdapter.OnAthcarClickListener() {


            @Override
            public void OnItemClick(Athcar atchar) {
                startActivity(new Intent(view.getContext(), PrayersDetails.class).putExtra(AthcarOfAblutionAndPrayer.ATHCHAR_INTENT_TAG,atchar));
            }

            @Override
            public void OnImageFavoriteClick(Athcar atchar,int position) {
                athcarArrayList.remove(position);
                adapter.notifyItemRemoved(position);
            }
        });
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        imageViewMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), SalahTimeActivity.class));
            }
        });


    }

}
